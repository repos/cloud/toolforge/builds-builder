#!/usr/bin/env bash
set -x
set -o nounset
set -o errexit
set -o pipefail


# this is the library that yq uses, but yq has a bug for toml nested arrays: https://github.com/mikefarah/yq/issues/1758
# so as a workaround we parse toml -> json -> jq edits -> json -> toml
JSON_TOML_URL=https://github.com/pelletier/go-toml/releases/download/v2.1.1/jsontoml_2.1.1_linux_amd64.tar.xz
TOML_JSON_URL=https://github.com/pelletier/go-toml/releases/download/v2.1.1/tomljson_2.1.1_linux_amd64.tar.xz
JQ_URL=https://github.com/jqlang/jq/releases/download/jq-1.7.1/jq-linux-amd64


download_parsing_tools() {
    wget -O - "$JSON_TOML_URL" | tar xvJ
    wget -O - "$TOML_JSON_URL" | tar xvJ
    wget -O jq "$JQ_URL" && chmod +x jq
}

download_buildpack(){
    local url="${1?}"
    local dest_dir="${2?}"
    local tmpdir

    tmpdir="$(mktemp -d)"
    cd "$tmpdir"
    wget "$url"
    unzip -- *.zip
    mkdir -p "$dest_dir/"
    cp -a -- */* "$dest_dir/"
    cd -
    rm -rf "$tmpdir"
}


add_entry_to_all_groups() {
    local toml_file="${1?}"
    local group_entry="${2?}"

    # Poor-dev's toml parser xd, note the '|' as separator to avoid collisions, don't change that
    # TODO: use a proper parser -.-
    # TODO: avoid adding also to the already existing nodejs buildpack
    sed -i "s|\[\[order\]\]|$group_entry|" "$toml_file"
}


add_new_group() {
    # Use as:
    #   add_new_group /path/to/toml <<EOT
    #   [[order]]
    #       [[order.group]]
    #       id = ....
    #   EOT
    #
    # The new group will be added to the start of the toml file
    local toml_file="${1?}"
    local tmpdir

    tmpdir="$(mktemp -d)"
    local tmp_toml="$tmpdir/tmp.toml"
    cat > "$tmp_toml"
    cat "$toml_file" >> "$tmp_toml"
    cp "$tmp_toml" "$toml_file"
    chmod 644 "$toml_file"
    rm -rf "$tmpdir"
}


inject_dotnet() {
    local buildpacks_dir="${1?No buildpacks_dir passed}"
    local order_toml="${2?No order_toml passed}"
    download_buildpack \
        "https://gitlab.wikimedia.org/repos/cloud/toolforge/buildpacks/dotnetcore-buildpack/-/archive/move_to_api_0.10/dotnetcore-buildpack-move_to_api_0.10.zip" \
        "$buildpacks_dir/jincod_dotnetcore-buildpack/7.0.401/"

    local procfile_version
    procfile_version="$(ls "$buildpacks_dir/heroku_procfile")"
    add_new_group "$order_toml" <<EOT
[[order]]
    [[order.group]]
    id = "jincod/dotnetcore-buildpack"
    version = "7.0.401"
    api = "0.10"
    optional = false
    [[order.group]]
    id = "heroku/procfile"
    version = "$procfile_version"
    optional = true
EOT
}


inject_rust() {
    local buildpacks_dir="${1?No buildpacks_dir passed}"
    local order_toml="${2?No order_toml passed}"
    download_buildpack \
        "https://gitlab.wikimedia.org/repos/cloud/toolforge/buildpacks/rust-buildpack/-/archive/move_to_api_0.10/rust-buildpack-move_to_api_0.10.zip" \
        "$buildpacks_dir/emk_rust/0.1/"

    local procfile_version
    procfile_version="$(ls "$buildpacks_dir/heroku_procfile")"
    # we need to add the procfile one as it's not included in the rust buildpack
    add_new_group "$order_toml" <<EOT
[[order]]
    [[order.group]]
    id = "emk/rust"
    version = "0.1"
    api = "0.10"
    optional = false
    [[order.group]]
    id = "heroku/procfile"
    version = "$procfile_version"
    optional = true
EOT
}


# We can remove this once they add the buildpack back to the builder
#    https://github.com/heroku/buildpacks-jvm/pull/587
inject_clojure() {
    local buildpacks_dir="${1?No buildpacks_dir passed}"
    local order_toml="${2?No order_toml passed}"
    download_buildpack \
        "https://gitlab.wikimedia.org/repos/cloud/toolforge/buildpacks/clojure-buildpack/-/archive/move_to_api_0.10/clojure-buildpack-move_to_api_0.10.zip" \
        "$buildpacks_dir/heroku_clojure/0.1/"

    local procfile_version
    procfile_version="$(ls "$buildpacks_dir/heroku_procfile")"
    # we need to add the procfile one as it's not included in the clojure buildpack
    add_new_group "$order_toml" <<EOT
[[order]]
    [[order.group]]
    id = "heroku/clojure"
    version = "0.1"
    api = "0.10"
    optional = false
    [[order.group]]
    id = "heroku/procfile"
    version = "$procfile_version"
    optional = true
EOT
}


inject_aptfile() {
    local buildpacks_dir="${1?No buildpacks_dir passed}"
    local order_toml="${2?No order_toml passed}"

    download_buildpack \
        "https://gitlab.wikimedia.org/repos/cloud/toolforge/buildpacks/apt-buildpack/-/archive/move_to_api_0.10/apt-buildpack-move_to_api_0.10.zip" \
        "$buildpacks_dir/fagiani_apt/0.2.5/"

    local aptfile_group='[[order]]\
    [[order.group]]\
    id = "fagiani/apt"\
    version = "0.2.5"\
    optional = true\
    '
    add_entry_to_all_groups "$order_toml" "$aptfile_group"
}


inject_nodejs() {
    local buildpacks_dir="${1?No buildpacks_dir passed}"
    local order_toml="${2?No order_toml passed}"

    local version
    # the only subdirectory is the version that's installed
    version="$(ls "$buildpacks_dir/heroku_nodejs")"

    local nodejs_group='[[order]]\
    [[order.group]]\
    id = "heroku/nodejs"\
    version = "'"$version"'"\
    optional = true\
    '
    add_entry_to_all_groups "$order_toml" "$nodejs_group"
}


remove_group() {
    local toml_file="${1?}"
    local id="${2?}"
    local expected_length="${3?}"
    local tmpdir

    tmpdir="$(mktemp -d)"
    local tmp_toml="$tmpdir/tmp.toml"

    # filtering objects with length X (assisting buildpacks + our id, the easiest way I found to identify them)
    # and that have our buildpack id
    ./tomljson "$toml_file" \
    | ./jq "walk(if type==\"object\" and (.group|length)==$expected_length and .group[0].id==\"$id\" then empty else . end)" \
    | ./jsontoml >> "$tmp_toml"

    cp "$tmp_toml" "$toml_file"
    chmod 644 "$toml_file"
    rm -rf "$tmpdir"
}


put_nodejs_at_the_end() {
    local buildpacks_dir="${1?No buildpacks_dir passed}"
    local order_toml="${2?No order_toml passed}"

    local version
    # the only subdirectory is the version that's installed
    version="$(ls "$buildpacks_dir/heroku_nodejs")"

    local procfile_version
    procfile_version="$(ls "$buildpacks_dir/heroku_procfile")"

    # we expect it to have only procfile + nodejs
    remove_group "$order_toml" "heroku/nodejs" 2

    cat >>"$order_toml" <<EOT
[[order]]
    [[order.group]]
    id = "heroku/nodejs"
    version = "$version"
    optional = false
    [[order.group]]
    id = "heroku/procfile"
    version = "$procfile_version"
    optional = true
EOT
}


inject_locales() {
    local buildpacks_dir="${1?No buildpacks_dir passed}"
    local order_toml="${2?No order_toml passed}"

    download_buildpack \
        "https://gitlab.wikimedia.org/repos/cloud/toolforge/buildpacks/locale-buildpack/-/archive/move_to_api_0.10/dotnetcore-buildpack-move_to_api_0.10.zip" \
        "$buildpacks_dir/heroku-community_locale/0.1/"

    local nodejs_group='[[order]]\
    [[order.group]]\
    id = "heroku-community/locale"\
    version = "0.1"\
    optional = true\
    '
    add_entry_to_all_groups "$order_toml" "$nodejs_group"
}



main() {
    local buildpacks_dir="/tmp_builder/buildpacks"
    local order_toml="/tmp_builder/order.toml"

    # For toml parsing, includes jq
    download_parsing_tools

    # as we support multistack with nodejs, any lang+nodejs combination should have
    # priority over only nodejs
    put_nodejs_at_the_end "$buildpacks_dir" "$order_toml"


    # Enable users to build dotnet/mono/c# code
    inject_dotnet "$buildpacks_dir" "$order_toml"
    # Enable users to build rust
    inject_rust "$buildpacks_dir" "$order_toml"
    # Enable users to build clojure
    inject_clojure "$buildpacks_dir" "$order_toml"

    # these are appended to existing order groups rather than creating new ones
    # to allow multi-buildpack
    inject_locales "$buildpacks_dir" "$order_toml"
    inject_nodejs "$buildpacks_dir" "$order_toml"
    inject_aptfile "$buildpacks_dir" "$order_toml"

    echo "<------ Final order.toml"
    cat "$order_toml"
}


main "$@"
