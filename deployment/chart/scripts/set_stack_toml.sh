#!/usr/bin/env bash

set -o nounset
set -o errexit
set -o pipefail

main() {
    local run_image="${RUN_IMAGE?No run image passed, please set an environment variable RUN_IMAGE}"
    echo "Using run image: $run_image"
    cat <<EOS >/layers/stack.toml
[run-image]
  image = "$run_image"
EOS
}

main "$@"
