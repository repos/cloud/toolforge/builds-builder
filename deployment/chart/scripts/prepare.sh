#!/usr/bin/env bash
set -e

echo "Running on node $MY_NODE_NAME"

if [[ "$(workspaces.cache.bound)" == "true" ]]; then
  echo "> Setting permissions on '$(workspaces.cache.path)'..."
  chown -R "$(params.USER_ID):$(params.GROUP_ID)" "$(workspaces.cache.path)"
fi

for path in "/tekton/home" "/layers" "$(workspaces.source.path)"; do
  echo "> Setting permissions on '$path'..."
  chown -R "$(params.USER_ID):$(params.GROUP_ID)" "$path"

  if [[ "$path" == "$(workspaces.source.path)" ]]; then
    chmod 775 "$(workspaces.source.path)"
  fi
done

echo "> Parsing additional configuration..."
parsing_flag=""
envs=()
for arg in "$@"; do
  if [[ "$arg" == "--env-vars" ]]; then
    echo "-> Parsing env variables..."
    parsing_flag="env-vars"
  elif [[ "$parsing_flag" == "env-vars" ]]; then
    envs+=("$arg")
  fi
done

echo "> Processing any environment variables..."
ENV_DIR="/platform/env"

echo "--> Creating 'env' directory: $ENV_DIR"
mkdir -p "$ENV_DIR"

for env in "${envs[@]}"; do
  IFS='=' read -r key value _ <<< "$env"
  if [[ "$key" != "" && "$value" != "" ]]; then
    path="${ENV_DIR}/${key}"
    echo "--> Writing ${path}..."
    echo -n "$value" > "$path"
  fi
done
