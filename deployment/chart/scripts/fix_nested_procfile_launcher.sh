#!/usr/bin/env bash

set -e

LAYERS_DIR="/layers"
PROCFILE_LAYER="$LAYERS_DIR/heroku_procfile"
FIX_LAYER="$PROCFILE_LAYER/nested_launcher_fix"
ENV_DIR="$FIX_LAYER/env"

if ! [[ -e "$PROCFILE_LAYER" ]]; then
    echo "Skipping, procfile buildpack not detected..."
    exit 0
fi

# Allow procfile launcher to run itself
# T355214
mkdir -p "$ENV_DIR"

cnb_platform="${ENV_DIR}/CNB_PLATFORM_API"
# shellcheck disable=SC2153 # possible misspelling of unassigned variable
echo "--> Writing ${cnb_platform}=$CNB_PLATFORM ..."
echo -n "$CNB_PLATFORM_API" > "$cnb_platform"

if [[ -e "$LAYERS_DIR/heroku_go" ]]; then
    # golang entrypoints have the same name as the binaries
    # so we put the /cnb/process at the end of the path to avoid
    # recursive calls
    #    See https://phabricator.wikimedia.org/T363417
    path_var="${ENV_DIR}/PATH.append"
    echo "--> Writing ${path_var}..."
    echo -n ":/cnb/process:/cnb/lifecycle" > "$path_var"
else
    path_var="${ENV_DIR}/PATH.prepend"
    echo "--> Writing ${path_var}..."
    echo -n "/cnb/process:/cnb/lifecycle:" > "$path_var"
fi

cat >"$FIX_LAYER.toml"<<EOL
[types]
launch = true
build = false
cache = false
EOL
