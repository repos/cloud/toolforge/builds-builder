#!/usr/bin/env bash
set -e
set -o pipefail

explain_harbor_quota_error() {
    # not the best approach but we have to somehow avoid false positives
    expected_error_str="will exceed the configured upper limit"
    last_output=$(tail -n 1 <<< "$1")
    if [[ $last_output == *"$expected_error_str"* ]]; then
        echo -e "\033[33m|=================================================================|\033[0m"
        echo -e "\033[33m| The above error is likely because the build is bigger than your |\033[0m"
        echo -e "\033[33m| current available quota.                                        |\033[0m"
        echo -e "\033[33m| To check your available quota run: \"toolforge build quota\".     |\033[0m"
        echo -e "\033[33m| To free up space you can run: \"toolforge build clean\".          |\033[0m"
        echo -e "\033[33m| If the error persists,                                          |\033[0m"
        echo -e "\033[33m| please report to the Toolforge admins: https://w.wiki/6Zuu      |\033[0m"
        echo -e "\033[33m|=================================================================|\033[0m"
    fi
    exit 1
}

output=$(/cnb/lifecycle/exporter "$@" 2>&1 | tee /dev/fd/2) || explain_harbor_quota_error "$output"
