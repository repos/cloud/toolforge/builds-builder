#!/usr/bin/env bash
set -e
grep "digest" /layers/report.toml | cut -d'"' -f2 | cut -d'"' -f2 | tr -d '\n' > "$(results.APP_IMAGE_DIGEST.path)"
# shellcheck disable=SC2005  # params.APP_IMAGE is a variable so echo is needed
echo "$(params.APP_IMAGE)" > "$(results.APP_IMAGE_URL.path)"
echo "Built image $(cat "$(results.APP_IMAGE_URL.path)")@$(cat "$(results.APP_IMAGE_DIGEST.path)")"
