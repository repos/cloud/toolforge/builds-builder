#!/usr/bin/env bash
# See https://phabricator.wikimedia.org/T370610
# and https://help.heroku.com/RFDJQSG3/how-can-i-override-imagemagick-settings-in-a-policy-xml-file
#
# heroku-22 base image comes with a default imagemagick config under /etc/ImageMagick-6/policy.xml file that
# restricts the sizes it can manage, as we are running inside a container we don't really want to limit them here,
# and allow the container to use as much memory as it can.

set -o nounset
set -o errexit
set -o pipefail

LAYERS_DIR="/layers"
# ffmpeg can only be installed through the apt buildpack, so we depend on that one
PROCFILE_LAYER="$LAYERS_DIR/fagiani_apt"
FIX_LAYER="$PROCFILE_LAYER/imagemagick_fix"
ENV_DIR="$FIX_LAYER/env"
MAGICK_CONFIGURE_PATH="/app/.magick:$FIX_LAYER:/etc/ImageMagick-6"

if ! [[ -e "$PROCFILE_LAYER" ]]; then
    echo "Skipping, apt buildpack not detected..."
    exit 0
fi

mkdir -p "$ENV_DIR"

magick_configure_varfile="${ENV_DIR}/MAGICK_CONFIGURE_PATH"
echo "--> Writing MAGICK_CONFIGURE_PATH=$MAGICK_CONFIGURE_PATH ..."
echo -n "$MAGICK_CONFIGURE_PATH" > "$magick_configure_varfile"

echo "--> Writing $FIX_LAYER/policy.xml ..."
# We need to set some values, otherwise it falls back to the default config file
cat <<EOC >"$FIX_LAYER/policy.xml"
<policymap>
  <policy domain="resource" name="memory" value="1PiB"/>
  <policy domain="resource" name="map" value="1PiB"/>
  <policy domain="resource" name="width" value="1PP"/>
  <policy domain="resource" name="height" value="1PP"/>
  <policy domain="resource" name="area" value="1PP"/>
  <policy domain="resource" name="disk" value="1PiB"/>
  <policy domain="delegate" rights="none" pattern="URL" />
  <policy domain="delegate" rights="none" pattern="HTTPS" />
  <policy domain="delegate" rights="none" pattern="HggTTP" />
  <policy domain="path" rights="none" pattern="@*"/>
  <policy domain="cache" name="shared-secret" value="passphrase" stealth="true"/>
</policymap>
EOC

cat >"$FIX_LAYER.toml"<<EOL
[types]
launch = true
build = false
cache = false
EOL
