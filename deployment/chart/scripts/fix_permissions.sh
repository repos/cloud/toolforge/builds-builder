#!/usr/bin/env bash
set -e

# this is needed for the user that will run in the container to write on the app source code
# usually for hardcoded paths for buildpacks that expect to run as user 1000
# like logs, on-the-fly conf files, ...
for path in "/layers" "$(workspaces.source.path)"; do
  echo "> Setting permissions on '$path'..."
  chmod go+rw -R "$path"
  find "$path" -executable -exec chmod go+x {} \;
done
