#!/usr/bin/env python3
from pathlib import Path
from subprocess import STDOUT, CalledProcessError, check_output

import click
import yaml

CLI = "podman"

# these images tags get overwritten upstream, so depending on the time you pull it you get different ones
SKIP_IMAGES = {"runImage"}


def run(cmd: list[str], *, noop: bool):
    if noop:
        click.echo(f"Would run: {cmd}")
    else:
        return check_output(cmd, stderr=STDOUT)


@click.command()
@click.option("--noop", is_flag=True, default=False)
@click.option("--do-all", is_flag=True, default=False)
def main(do_all: bool, noop: bool):
    values = yaml.safe_load(Path("deployment/chart/values.yaml").read_text())
    for image_name, image_data in values["imagesSource"].items():
        if do_all or image_name not in SKIP_IMAGES:
            run([CLI, "pull", image_data["original"]], noop=noop)
            run(
                [CLI, "tag", image_data["original"], image_data["toolforge"]],
                noop=noop,
            )
            try:
                run([CLI, "push", image_data["toolforge"]], noop=noop)
            except CalledProcessError as error:
                if "configured as immutable" in error.output.decode("utf-8"):
                    click.echo(
                        f"It seems image {image_name} ({image_data['toolforge']}) is already uploaded, skipping"
                    )
                else:
                    click.echo(f"Error when trying to push {image_name}")
                    click.echo(error.output.decode("utf-8"))
                    raise

        else:
            click.echo(
                f"SKIP: skipping image {image_name}, as it might not be safe to re-push, if you want to push it "
                "anyhow, pass --do-all."
            )


if __name__ == "__main__":
    main()
