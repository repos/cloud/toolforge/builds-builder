#!/usr/bin/env python3
import os
import sys
from typing import Any, Dict, Tuple

import requests

robot_account = {
    "name": "local-image-builder",
    "secret": "Dummyr0botpass",
    "disable": False,
    "level": "system",
    "duration": -1,
    "description": "Tekton pipelines pushing robot account",
    "permissions": [
        {
            "access": [
                {"action": "delete", "resource": "artifact"},
                {"action": "list", "resource": "artifact"},
                {"action": "read", "resource": "artifact"},
                {"action": "create", "resource": "artifact-label"},
                {"action": "delete", "resource": "artifact-label"},
                {"action": "pull", "resource": "repository"},
                {"action": "push", "resource": "repository"},
                {"action": "list", "resource": "repository"},
                {"action": "delete", "resource": "repository"},
                {"action": "create", "resource": "scan"},
                {"action": "stop", "resource": "scan"},
                {"action": "create", "resource": "tag"},
                {"action": "delete", "resource": "tag"},
                {"action": "list", "resource": "tag"},
            ],
            "kind": "project",
            "namespace": "*",
        }
    ],
}

user_account = {
    "username": "maintain-harbor",
    "email": "maintain-harbor@wikimedia.org",
    "realname": "maintain-harbor",
    "password": "Dummyuserpass0",
    "comment": "",
}

toolforge_project = {
    "project_name": "toolforge",
    "public": True,
    "storage_limit": -1,
}

toolforge_immutability_tag_rule = {
    "priority": 0,
    "disabled": False,
    "action": "immutable",
    "template": "immutable_template",
    "scope_selectors": {
        "repository": [
            {"decoration": "repoMatches", "kind": "doublestar", "pattern": "**"}
        ]
    },
    "tag_selectors": [
        {"decoration": "excludes", "kind": "doublestar", "pattern": "{*dev-*,*-dev*}"}
    ],
}


def get_object(object_url: str, auth: Tuple[str, str]) -> Dict[str, Any]:
    response = requests.get(url=object_url, auth=auth)
    response.raise_for_status()
    return response.json()


def ensure_object_exists(
    object_url: str, new_object: Dict, auth: Tuple[str, str], object_type: str
):
    maybe_object = None
    new_object_name = new_object.get(
        "name",
        new_object.get("username", new_object.get("project_name")),
    )
    for object in get_object(object_url=object_url, auth=auth):
        # robot accounts use "name" and user accounts use "username"
        # projects use "name" and new projects use "project_name"
        object_name = object.get(
            "name",
            object.get("username", object.get("project_name")),
        )
        if object_name.endswith(new_object_name):
            maybe_object = object
            break

    if maybe_object is None:
        response = requests.post(url=object_url, json=new_object, auth=auth)
        response.raise_for_status()

        # for some reason you need to create the robot account then set the secret seperately
        if object_url.endswith("robots"):
            object = response.json()
            secret_request_obj = {"secret": new_object["secret"]}
            requests.patch(
                url=f"{object_url}/{object['id']}",
                json=secret_request_obj,
                auth=auth,
            )

        print(f"{object_type} '{new_object_name}' created successfully.")
    else:
        print(f"{object_type} '{new_object_name}' already exists, skipping creation.")


def ensure_immutability_tag_rule_exists(
    url: str, new_rule: Dict[str, Any], auth: Tuple[str, str]
):
    maybe_rule = None
    for rule in get_object(object_url=url, auth=auth):
        # immutable tag rules contain some keys that we want to remove before comparing
        rule.pop("id", None)
        new_rule.pop("id", None)
        rule.pop("priority", None)
        new_rule.pop("priority", None)
        rule.pop("disabled", None)
        new_rule.pop("disabled", None)

        if rule == new_rule:
            maybe_rule = rule
            break

    if maybe_rule is None:
        response = requests.post(url=url, json=new_rule, auth=auth)
        response.raise_for_status()

        print("Immutable tag rule for toolforge project created successfully.")
    else:
        print(
            "Immutable tag rule for toolforge project already exists, skipping creation."
        )


def main() -> int:
    harbor_ip = os.environ.get("HARBOR_IP", "127.0.0.1")
    robots_url = f"http://{harbor_ip}/api/v2.0/robots"
    users_url = f"http://{harbor_ip}/api/v2.0/users"
    projects_url = f"http://{harbor_ip}/api/v2.0/projects"
    immutable_url = f"http://{harbor_ip}/api/v2.0/projects/{toolforge_project['project_name']}/immutabletagrules"
    admin_auth = ("admin", "Harbor12345")
    user_auth = (user_account["username"], user_account["password"])

    ensure_object_exists(
        object_url=robots_url,
        new_object=robot_account,
        auth=admin_auth,
        object_type="Robot Account",
    )
    ensure_object_exists(
        object_url=users_url,
        new_object=user_account,
        auth=admin_auth,
        object_type="User Account",
    )
    ensure_object_exists(
        object_url=projects_url,
        new_object=toolforge_project,
        auth=user_auth,
        object_type="Project",
    )
    ensure_immutability_tag_rule_exists(
        url=immutable_url, new_rule=toolforge_immutability_tag_rule, auth=user_auth
    )

    return 0


if __name__ == "__main__":
    sys.exit(main())
