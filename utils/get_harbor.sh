#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

CURDIR=$(realpath "$(dirname "$0")")
HARBOR_DIR="${HARBOR_DIR:-.harbor}"
HARBOR_VERSION=${HARBOR_VERSION:-v2.12.2}  # we use this for now
export HARBOR_HOSTNAME="${HARBOR_IP?"HARBOR_IP variable is not set"}"

[[ -e $HARBOR_DIR ]] || mkdir -p "$HARBOR_DIR"
cd "$HARBOR_DIR"
wget \
    https://github.com/goharbor/harbor/releases/download/"${HARBOR_VERSION}"/harbor-online-installer-"${HARBOR_VERSION}".tgz \
    -O harbor-"${HARBOR_VERSION}".tgz
tar xvzf  harbor-"${HARBOR_VERSION}".tgz
cd harbor
"$CURDIR"/parse_harbor_config.py . "${HARBOR_DATA_DIR:-$PWD/data}"
./prepare
# the above runs a docker container and changes permissions for the data dirs,
# so the containers have the correct ones, but that forces us to use sudo when
# using docker-compose, sometimes you might want to put them out of the user
# home dir for security reasons

sudo "$(which docker-compose)" -f docker-compose.yml up -d

retries=30
health_url="http://$HARBOR_IP/api/v2.0/health"
while ! [[ "$(curl --silent --fail -H "Accept: application/json" "$health_url" | jq '.status' -r)" == "healthy" ]]; do
    echo "Waiting for harbor to come up..."
    sleep 5
    retries=$((retries - 1))
    if [[ $retries == "0" ]]; then
        echo "Timed out while waiting for harbor to come up!" >&2
        exit 1
    fi
done

"$CURDIR"/setup_harbor.py

echo -e "Now you can manage harbor with " \
    "'sudo $(which docker-compose) -f $PWD/docker-compose.yml ps', enjoy!" \
    "\nYou can also login into your new instance with user 'admin' and "\
    "password 'Harbor12345' on http://$HARBOR_IP"
